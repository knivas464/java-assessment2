package dxc;

class User
{
	private int id;
	private String email,username;
	private double walletBalance;
	
	public User(int id, String email, String username, double walletBalance) {
		super();
		this.id = id;
		this.email = email;
		this.username = username;
		this.walletBalance = walletBalance;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public double getWalletBalance() {
		return walletBalance;
	}

	public void setWalletBalance(double walletBalance) {
		this.walletBalance = walletBalance;
	}
	
	public boolean makePayment(double billAmount)
	{
		if (this.walletBalance>=billAmount)
		{
			this.walletBalance=this.walletBalance-billAmount;
			return true;
		}
		else
		{
			return false;
		}
	}
}
	
	class KYCUser extends User
	{
		private int rewardPoints;

		public KYCUser(int id, String email, String username, double walletBalance) {
			super(id, email, username, walletBalance);
			
		}

		public int getRewardPoints() {
			return rewardPoints;
		}

		public void setRewardPoints(int rewardPoints) {
			this.rewardPoints = rewardPoints;
		}
		
		public boolean makePayment(double billAmount)
		{
			boolean complete=super.makePayment(billAmount);
			if(complete)
			{
				rewardPoints=(int)(billAmount*0.1);
			}
			return complete;
		}
		
	}
	
	class EPayWallet 
	{
		static void PaymentByUser(User user,double billAmount)
		{
			if(user.makePayment(billAmount)==true)
			{
				System.out.println("Congratulations "+user.getUsername()+",payment of "+billAmount+" was successful");
				System.out.println("Your wallet balance is : "+user.getWalletBalance());
				
				if(user instanceof KYCUser)
				{
					System.out.println("Your reward points are : "+((KYCUser) user).getRewardPoints());
				}
				else
				{
					System.out.println("Insufficient wallet balance to make payment of"+billAmount);
					System.out.println("Your wallet balance is "+user.getWalletBalance());
				}
			}
		}
	}
	


public class OopsAssessment1 {

	public static void main(String[] args) {
		
		User u1=new User(101,"jack@dxc.com","Jack",1000);
		KYCUser u2=new KYCUser(201,"jill@dxc.com","Jill",3000);
		EPayWallet.PaymentByUser(u1,700);
		EPayWallet.PaymentByUser(u2, 2000);
	}

}
